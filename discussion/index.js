// Function
/* 
    Syntax:
        function functionName(){
            code block (statement)
        };
*/

function printName() {
  console.log("My name is Stanley!");
}

printName();

// Function declaration vs expressions

function declaredFunction() {
  console.log("Hello, world from declaredFunction()");
}

declaredFunction();

// Function expression
let variableFunction = function () {
  console.log("Hello again!");
};

variableFunction();

declaredFunction = function () {
  console.log("Updated declaredFunction");
};

const constantFunc = function () {
  console.log("Initialized with const!");
};

constantFunc();

/* 
constantFunc = function(){
  console.log("Cannot be reassigned");
constantFunc();
}
*/

// Function scoping

/* 
  Scope is the accessibility (visibility) of variables

  JS variables has 3 types of scope:
  1. local/block scope
  2. global scope
  3. function scope
*/

{
  let localVar = "Alonzo Mattheo";
  console.log(localVar);
}

let globalVar = "Aizaac Ellis";

console.log(globalVar);

function showNames() {
  // Function scoped variables
  var functionVar = "Joe";
  const functionConst = "John";
  let functionLet = "Jane";

  console.log(functionVar);
  console.log(functionConst);
  console.log(functionLet);
}

showNames();

// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

console.log("\n");

// Nested Functions

function myNewFunction() {
  let name = "Jane";

  function nestedFunction() {
    let nestedName = "John";
    console.log(name);
  }

  nestedFunction();
}

myNewFunction();

// Function and global scoped variables
let globalName = "Joy";

function myNewFunction2() {
  let nameInside = "Kenzo";
  console.log(globalName);
  console.log(nameInside);
}

myNewFunction2();

// console.log(nameInside);

// Using alert()

function showAlert() {
  alert("Hello, user!");
}

// showAlert();

// Prompts

// let promptName = prompt("Enter your name: ");
// console.log("Hello, " + promptName + "!");

function printWelcomeMessage() {
  let firstName = prompt("Enter your first name: ");
  let lastName = prompt("Enter your last name: ");

  console.log("Hello, " + firstName + " " + lastName + "!");
  console.log("Welcome to my page!");
}

printWelcomeMessage();

/* 
  Function Naming Conventions
    - Definitive names (verbs)
    - Avoid generic names
    - Avoid pointless and inappropriate names
    - Use camelcasing
    - Avoid JavaScript reserved keywords
*/
